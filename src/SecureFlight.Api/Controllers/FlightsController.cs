﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController(IService<Flight> flightService, IRepository<PassengerFlight> passengerFlightRepository, IMapper mapper)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await flightService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }
    [HttpPost]
    [Route("addPassenger")]
    public async Task<IActionResult> AddPassenger(long flightCode, string passengerId)
    {

        var passenger = (await passengerFlightRepository.FilterAsync(p => p.PassengerId == passengerId && p.FlightId == flightCode)).FirstOrDefault();
        if (passenger is not null)
        {
            return BadRequest();
        }
        var passengerFlight = new PassengerFlight() { FlightId = flightCode, PassengerId = passengerId };
        passengerFlightRepository.Add(passengerFlight);
       await  passengerFlightRepository.SaveChangesAsync();
        return Ok(passenger);
    }
    [HttpGet]
    [Route("{origin:string}/{destination:string}")]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> getByOriginDestination(string origin, string destination)
    {
        var flights = await flightService.FilterAsync(f => f.DestinationAirport == destination && f.OriginAirport == origin);
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }
}